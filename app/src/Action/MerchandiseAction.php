<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class MerchandiseAction
{
    private $view;
    private $logger;
    private $loginmanager;
    private $database;

    public function __construct(Twig $view, LoggerInterface $logger, $loginmanager, $database)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->loginmanager = $loginmanager;
        $this->database = $database;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Merchandise :: init");

        if(!$this->loginmanager->isLoggedIn()) {
            $this->logger->info("Register :: not logged in");

            return $response->withRedirect('/login');
        }

        if($request->getMethod() == 'POST') {
            return $this->processSave($request, $response, $args);
        }

        $args['isLoggedIn'] = $this->loginmanager->isLoggedIn();
        $args['isAdminUser'] = $this->loginmanager->isAdminUser();

        $conferenceMapper = $this->database->mapper('Entity\Conference');
        $conferences = $conferenceMapper->where(['slug' => $args['conferenceSlug']]);

        if(!$conferences->count()) {
            return $response->withRedirect("/");
        }

        $args['conference'] = $conferences->first();

        $baseRegistration = 
            ['dn' => $this->loginmanager->getCurrentUser(),
            'conference_id' => $args['conference']->id];

        $merchandiseMapper = $this->database->mapper('Entity\Merchandise');
        $reservationMapper = $this->database->mapper('Entity\MerchandiseReservation');

        $confMerchandise = $merchandiseMapper->all()->where(['conference_id' => $args['conference']->id]);
        $displayMerchandise = [];
        foreach($confMerchandise as $merchandiseItem) {
            $displayMerchandise[] = $merchandiseItem;
        }

        $args['displayMerchandise'] = $displayMerchandise;

        $args['displayMerchandiseOptions'] = [];
        foreach($displayMerchandise as $merch) {
            $args['displayMerchandiseOptions'][$merch->id] = json_decode($merch->variations);
        }

        $args['orderedMerchandise'] = [];
        foreach($displayMerchandise as $merch) {
            $orderedMerchItem = $reservationMapper->all()->where(['conference_id' => $args['conference']->id, 'dn' => $this->loginmanager->getCurrentUser(), 'merch_id' => $merch->id ]);
            if ( $orderedMerchItem->count() > 0 ) {
                $args['orderedMerchandise'][$merch->id] = $orderedMerchItem->first()->variation;
            } else {
                $args['orderedMerchandise'] = false;
            }
        }

        // var_dump($args['displayMerchandiseOptions']); die();

        // $registrationMapper = $this->database->mapper('Entity\Registration');
        // $registrations = $registrationMapper->where($baseRegistration);

        // if($registrations->count()) {
        //     $registration = $registrations->first();
        // } else {
        //     $registration = $registrationMapper->build($baseRegistration);
        // }

        // $args['form'] = $this->formbuilder->buildForm(json_decode($args['conference']->form), json_decode($registration->data), [2,99]);

        $this->logger->info("Merchandise :: render");

        $this->view->render($response, 'merchandise.twig', $args);
        return $response;
    }

    public function processSave(Request $request, Response $response, $args)
    {
        $this->logger->info("Merchandise :: save");

        $conferenceMapper = $this->database->mapper('Entity\Conference');
        $conferences = $conferenceMapper->where(['slug' => $args['conferenceSlug']]);

        $reservationMapper = $this->database->mapper('Entity\MerchandiseReservation');

        if(!$conferences->count()) {
            return $response->withRedirect("/");
        }

        $args['conference'] = $conferences->first();

        switch($_POST['action']) {
            case 'Reserve':
                $baseReservation = [
                    'conference_id' => $args['conference']->id,
                    'dn' => $this->loginmanager->getCurrentUser(),
                    'merch_id' => $_POST['merch_id'],
                    'variation' => $_POST['variation']
                ];

                $reservation = $reservationMapper->build($baseReservation);
                $reservationMapper->save($reservation);

                return $response->withRedirect("/?reservationsuccess=true");
                
                break;

            case 'Update Reservation':
                $entity = $reservationMapper->first([
                    'conference_id' => $args['conference']->id,
                    'dn' => $this->loginmanager->getCurrentUser(),
                    'merch_id' => $_POST['merch_id'],
                ]);

                $entity->variation = $_POST['variation'];

                $reservationMapper->update($entity);

                return $response->withRedirect("/?reservationupdated=true");
                
                break;

            case 'Cancel Reservation':
                $entity = $reservationMapper->first([
                    'conference_id' => $args['conference']->id,
                    'dn' => $this->loginmanager->getCurrentUser(),
                    'merch_id' => $_POST['merch_id'],
                ]);

                $reservationMapper->delete($entity);

                return $response->withRedirect("/?reservationcancelled=true");
                
                break;
        }

        // $baseRegistration = [
        //     'dn' => ,
        //     'conference_id' => $args['conference']->id
        // ];

        // $registrationMapper = $this->database->mapper('Entity\Registration');
        // $registrations = $registrationMapper->where($baseRegistration);

        // if($registrations->count()) {
        //     $registration = $registrations->first();
        // } else {
        //     $registration = $registrationMapper->build($baseRegistration);
        // }

        // $registration->data = json_encode($request->getParsedBody());
        // $registration->cancelled = 0;

        // $registrationMapper->save($registration);

        // $this->logger->info("Register :: saved");

        // return $response->withRedirect(implode('', ["/?registrationsuccess=", $args['conference']->slug]));
    }
}
