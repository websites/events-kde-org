<?php
namespace Entity;
class Merchandise extends \Spot\Entity
{
    protected static $table = 'merchandise';
    public static function fields()
    {
        return [
            'id'                    => ['type' => 'integer', 'primary' => true, 'required' => true],
            'conference_id'         => ['type' => 'integer'],
            'title'                 => ['type' => 'string'],
            'description'           => ['type' => 'string'],
            'image'                 => ['type' => 'string'],
            'variations'            => ['type' => 'string'],
            'date_created' 			=> ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }
}