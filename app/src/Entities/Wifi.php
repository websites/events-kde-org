<?php
namespace Entity;
class Wifi extends \Spot\Entity
{
    protected static $table = 'wifi';
    public static function fields()
    {
        return [
            'id'                    => ['type' => 'integer', 'primary' => true, 'required' => true],
            'conference_id'         => ['type' => 'integer', 'primary' => false, 'required' => true],
            'username'              => ['type' => 'string', 'primary' => false, 'required' => false],
            'password'              => ['type' => 'string', 'primary' => false, 'required' => false],
            'registration_id'       => ['type' => 'integer', 'primary' => false, 'required' => false]
        ];
    }
}
