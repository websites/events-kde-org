<?php
namespace Entity;
class MerchandiseReservation extends \Spot\Entity
{
    protected static $table = 'reservation';
    public static function fields()
    {
        return [
            'id'                    => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'conference_id'         => ['type' => 'integer'],
            'dn'                    => ['type' => 'string'],
            'merch_id'              => ['type' => 'integer'],
            'variation'             => ['type' => 'string'],
            'date_created' 			=> ['type' => 'datetime', 'value' => new \DateTime()]
        ];
    }
}