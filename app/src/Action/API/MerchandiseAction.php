<?php
namespace App\Action\API;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class MerchandiseAction
{
    private $view;
    private $logger;
    private $loginmanager;
    private $formbuilder;
    private $database;

    public function __construct(Twig $view, LoggerInterface $logger, $formbuilder, $loginmanager, $database)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->formbuilder = $formbuilder;
        $this->loginmanager = $loginmanager;
        $this->database = $database;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("API :: Merchandise :: init");

        $response = $response->withHeader('Content-type', 'application/json');

        // Get Conference
        $conferenceMapper = $this->database->mapper('Entity\Conference');
        $results = $conferenceMapper->all()->where(['slug' => $args['conferenceSlug']]);

        if(!$results->count()) {
            return $this->failureResponse($response);
        }

        if(!$this->loginmanager->isLoggedIn()) {
            return $this->failureResponse($response);
        }

        $profileMapper = $this->database->mapper('Entity\Profile');

        $conference = $results->first();

        $merchandiseMapper = $this->database->mapper('Entity\Merchandise');
        $confMerchandise = $merchandiseMapper->all()->where(['conference_id' => $conference->id]);
        $displayMerchandise = [];
        foreach($confMerchandise as $merchandiseItem) {
            $displayMerchandise[] = $merchandiseItem;
        }

        $args['data'] = [
            'conference' => [
                'id' => $conference->id,
                'name' => $conference->name,
                'start_date' => $conference->start_date,
                'end_date' => $conference->end_date,
            ],
            'merchandise' => $displayMerchandise
        ];

        $this->logger->info("API :: Merchandise :: render");
        
        $this->view->render($response, 'api/merchandise.twig', $args);
        return $response;   
    }

    private function failureResponse($response) {
        $args['data'] = [
            'success' => false
        ];

        $this->view->render($response, 'api/merchandise.twig', $args);
        return $response;
    }
}
