<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class StatsAction
{
    private $view;
    private $logger;
    private $loginmanager;
    private $formbuilder;
    private $database;

    public function __construct(Twig $view, LoggerInterface $logger, $formbuilder, $loginmanager, $database)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->formbuilder = $formbuilder;
        $this->loginmanager = $loginmanager;
        $this->database = $database;
    }

    public function __invoke(Request $request, Response $response, $args)
    {
        $this->logger->info("Stats :: init");

        if(!$this->loginmanager->isLoggedIn()) {
            $this->logger->info("Stats :: not logged in");

            return $response->withRedirect('/login');
        }

        $args['isLoggedIn'] = $this->loginmanager->isLoggedIn();

        $conferenceMapper = $this->database->mapper('Entity\Conference');
        $conferences = $conferenceMapper->where(['slug' => $args['conferenceSlug']]);

        if(!$conferences->count()) {
            return $response->withRedirect("/");
        }

        $adminMapper = $this->database->mapper('Entity\Admin');
        if(!$adminMapper->where([
                'conference_id' => $conferences->first()->id,
                'dn' => $this->loginmanager->getCurrentUser()
            ])->count()) {
        
            $this->logger->info("Stats :: not an admin user");

            return $response->withRedirect('/?access=denied');   
        }

        $args['conference'] = $conferences->first();

        $registrationMapper = $this->database->mapper('Entity\Registration');
        $registrations = $registrationMapper->all()->where(['conference_id' => $args['conference']->id]);

        $profileMapper = $this->database->mapper('Entity\Profile');
        $wifiMapper = $this->database->mapper('Entity\Wifi');

        $args['registrationdata'] = [];
        $args['registrationcount'] = $registrations->count();
        foreach($registrations as $registration) {
            $regdata = (array)json_decode($registration->data);
            foreach($regdata as $key => $reg) {
                if(is_object($reg)) {
                    foreach((array)$reg as $k => $v) {
                        $regdata[implode("_", [$key, $k])] = $v;
                    }
                    unset($regdata[$key]);
                }
                unset($regdata['Save']);
            }

            if($args['conference']->wifi == 1) {
                $wifiData = $wifiMapper->where(['conference_id' => $args['conference']->id, 'registration_id' => $registration->id])->first();
                $regdata['wifi_username'] = $wifiData->username;
                $regdata['wifi_password'] = $wifiData->password;
            }

            $args['registrationdata'][] = array_merge(['dn' => $registration->dn], $regdata);
        }

        $profiles = $profileMapper->all()->where(['dn' => array_map(function($data) {
            return $data['dn'];
        }, $args['registrationdata'])]);

        $args['profiledata'] = [];
        foreach($profiles as $profile) {
            $profiledata = (array)json_decode($profile->data);
            if(isset($profiledata['Other_Roles_in_KDE'])) {
                $profiledata['Other_Roles_in_KDE'] = implode(", ", array_keys((array)$profiledata['Other_Roles_in_KDE']));
            }

            if(isset($profiledata['Dietary_requirements_and_allergies'])) {
                $profiledata['Dietary_requirements_and_allergies'] = implode(", ", array_keys((array)$profiledata['Dietary_requirements_and_allergies']));
            }

            unset($profiledata['Save']);

            $args['profiledata'][] = array_merge(['dn' => $profile->dn], $profiledata);
        }

        /**
         * Merch (T-Shirt) Pre-Orders
         */
        $merchReservationMapper = $this->database->mapper('Entity\MerchandiseReservation');
        $merchReservations = $merchReservationMapper->all()->where(['conference_id' => $args['conference']->id]);
        $args['merchReservations'] = [];

        $sizes = [];
        foreach($merchReservations as $reservation) {
            $args['merchReservations'][] = $reservation; //fill array with data from model

            if(!isset($sizes[$reservation->variation])) {
                $sizes[$reservation->variation] = 0;
            }
            $sizes[$reservation->variation]++;
        }
        $args['sizes'] = $sizes;
        $args['sizelist'] = json_encode(array_keys($sizes));
        $args['sizevalues'] = json_encode(array_values($sizes));

        $countries = [];
        $primaryroles = [];
        $dietaryrequirements = [];
        foreach($args['profiledata'] as $registration) {
            if($registration['Country'] != 'Please Select Country' && !isset($countries[$registration['Country']])) {
                $countries[$registration['Country']] = 0;
            }

            if(!isset($primaryroles[$registration['Primary_Role_in_KDE']])) {
                $primaryroles[$registration['Primary_Role_in_KDE']] = 0;
            }

            if(isset($registration['Dietary_requirements_and_allergies'])) {
                foreach(explode(", ", $registration['Dietary_requirements_and_allergies']) as $requirement) {
                    if(strlen($requirement)) {
                        if(!isset($dietaryrequirements[$requirement])) {
                            $dietaryrequirements[$requirement] = 0;
                        }
    
                        $dietaryrequirements[$requirement]++;
                    }
                }
            }

            if($registration['Country'] != 'Please Select Country') {
                $countries[$registration['Country']]++;
            }

            $primaryroles[$registration['Primary_Role_in_KDE']]++;
        }

        $args['countrylist'] = json_encode(array_keys($countries));
        $args['countryvalues'] = json_encode(array_values($countries));
        $args['countryborders'] = json_encode(array_fill(0, count($countries), '#666666'));
        $args['countrycolours'] = [];
        foreach($countries as $country) {
            $args['countrycolours'][] = "rgb(200," . ($country * 25) . "," . ($country * 20) . ")";
        }
        $args['countrycolours'] = json_encode($args['countrycolours']);

        $args['primaryrolelist'] = json_encode(array_keys($primaryroles));
        $args['primaryrolevalues'] = json_encode(array_values($primaryroles));
        $args['primaryroleborders'] = json_encode(array_fill(0, count($primaryroles), '#666666'));
        $args['primaryrolecolours'] = [];
        foreach($primaryroles as $primaryrole) {
            $args['primaryrolecolours'][] = "rgb(200," . ($primaryrole * 25) . "," . ($primaryrole * 20) . ")";
        }
        $args['primaryrolecolours'] = json_encode($args['primaryrolecolours']);

        $args['dietaryrequirementslist'] = json_encode(array_keys($dietaryrequirements));
        $args['dietaryrequirementsvalues'] = json_encode(array_values($dietaryrequirements));
        $args['dietaryrequirementsborders'] = json_encode(array_fill(0, count($dietaryrequirements), '#666666'));
        $args['dietaryrequirementscolours'] = [];
        foreach($dietaryrequirements as $dietaryrequirement) {
            $args['dietaryrequirementscolours'][] = "rgb(200," . ($dietaryrequirement * 25) . "," . ($dietaryrequirement * 20) . ")";
        }
        $args['dietaryrequirementscolours'] = json_encode($args['dietaryrequirementscolours']);

        $args['registrationkeys'] = array_keys($args['registrationdata'][0]);

        $this->logger->info("Stats :: render");

        $this->view->render($response, 'stats.twig', $args);
        return $response;
    }
}

